FROM node:21 as build

# ENV NODE_ENV=production

WORKDIR /app

COPY package.json ./
COPY package-lock.json ./
RUN npm install
COPY . ./
RUN npm run build


FROM caddy

WORKDIR /app
COPY --from=build /app/dist /usr/share/caddy

EXPOSE 80
